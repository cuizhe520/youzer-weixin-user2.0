// pages/order/order.js
var app = getApp();
// app.tofixed();
Page({
  data: {
    list: [
      "点餐",
      "商家详情",
      "用户评价"
    ],
    imgUrl: "../../asset/image/icon_ms_gw.png",
    dishType: "",//菜品类别
    select: "0",//点餐和商家的样式切换
    select1: "0",//菜品类别列表切换  
    select2: "1",//点击购物车弹出详情切换 0弹出 1收回
    dishList: [//菜品类别
    ],
    totalPrice: "0.00",
    payDish: [],//需要购买的菜品
    dishDetails: [//菜品列表
      { id: 1, name: "宫保鸡丁", price: "12.0", dishType: "热菜", Url: "../../asset/image/0.jpg" }
    ],
    merchantDetails: {},
    tapNum:0
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    var that = this;
    //获取菜品详情
    var jsonValue = {
      command: "GetAllTypeGoods",
      merchantId: options.merchantId
    };
    wx.setStorageSync('merchantid', options.merchantId);
    var sendValue = JSON.stringify(jsonValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: sendValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          for (var i = 0, dishList = []; i < res.data.data.typeGoods.length; i++) {
            dishList[i] = res.data.data.typeGoods[i].typeName;
          }
          that.setData({ dishList: dishList });
          that.setData({ dishDetails: res.data.data.typeGoods });
          that.setData({ dishType: res.data.data.typeGoods[0].typeName })
        }

      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
    //获取商家详情
    var jsonValue = {
      command: "merchantDetail",
      merchantId: options.merchantId
    };
    var sendJson = JSON.stringify(jsonValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: sendJson
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          that.setData({ merchantDetails: res.data.data.merchantDetail })
          wx.setStorageSync('merchantName', res.data.data.merchantDetail.merchantName);
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  onReady: function () {
    // 页面渲染完成
    this.setData({ dishType: this.data.dishList[0] });
    this.setData({ payDish: wx.getStorageSync('disharr') || [] });
    this.countPrice();
    this.cartImg();
  },
  onShow: function () {
    // 页面显示
    this.setData({ tapNum: 0 })
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
    wx.setStorageSync('disharr', []);
  },
  tap001: function (e) {//点餐。商家切换
    const index = e.currentTarget.dataset.index;
    if (index == 2) {
      wx.showModal({
        title: '提示',
        content: '用户评价暂未接入到小程序，请使用悠择APP评论',
        showCancel: false
      });
      return;
    }
    this.setData({ select: index });
  },
  tap002: function (e) {//菜品类别切换
    const index = e.currentTarget.dataset.index;
    this.setData({ select1: index, dishType: this.data.dishList[index] });
  },
  tap003: function (e) {//点击加号
    //const index = e.currentTarget.dataset.index;
    var xxx = e.currentTarget.dataset.name;
    var price = e.currentTarget.dataset.price;
    var score = e.currentTarget.dataset.score;
    var goodsid = e.currentTarget.dataset.goodsid;
    var arr, arr1;
    var bool = 0;
    var j = 0;
    //console.log(this.data);
    for (var i = 0, length = this.data.payDish.length; i < length; i++) {
      if (this.data.payDish[i].name == xxx) {
        j = this.data.payDish[i].num;
        j = Number(j);
        j++;
        arr = this.data.payDish[i];
        arr.num = j;
        wx.setStorageSync('disharr', this.data.payDish);
        this.setData({ payDish: wx.getStorageSync('disharr') });
        bool = 1;
      }
    }
    if (!bool) {
      j++;
      var newArray = [{ name: xxx, num: j, price: price, score: score, goodsid: goodsid, activityTypeId: 0, activityNo: 0 }];
      arr1 = this.data.payDish;
      arr1 = arr1.concat(newArray);
      wx.setStorageSync('disharr', arr1)
      this.setData({ payDish: wx.getStorageSync('disharr') });
      //this.setData({payDish: arr1 });
      console.log("添加")
    }
    this.countPrice();
    this.cartImg();
  },
  tap004: function (e) {//点击减号
    var xxx = e.currentTarget.dataset.name;
    var arr;
    var j = 0;
    var select = this.data.select2;
    //console.log("减号");
    for (var i = 0, length = this.data.payDish.length; i < length; i++) {
      if (this.data.payDish[i].name == xxx) {
        j = this.data.payDish[i].num;
        j = Number(j);
        j--;
        arr = this.data.payDish[i];
        if (j == 0) {
          this.data.payDish.splice(i, 1);
        } else {
          arr.num = j;
        }
        wx.setStorageSync('disharr', this.data.payDish)
        this.setData({ payDish: wx.getStorageSync('disharr') });
        //this.setData({payDish: this.data.payDish });
        //console.log("删除")
        break;
      }
    }
    if (this.data.payDish.length == 0) {
      this.setData({ select2: '1' })
    };
    this.countPrice();
    this.cartImg();
  },
  cartImg: function () {
    if (this.data.payDish.length != 0) {
      this.setData({ imgUrl: "../../asset/image/icon_ms_gw.png" })
    } else {
      this.setData({ imgUrl: "../../asset/image/ShoppingCart.png" })
    }
  },
  onPullDownRefresh: function () {//禁止下拉刷新
    wx.stopPullDownRefresh();
  },
  alert: function () {//购物车详情
    if (this.data.payDish.length == 0) {
      return;
    }
    var select = this.data.select2;
    console.log(select);
    if (select == 0) {
      this.setData({ select2: '1' })
    } else {
      this.setData({ select2: '0' })
    }
  },
  clear: function () {//清空购物车
    var that = this;
    wx.showModal({
      title: '温馨提示',
      content: '清空购物车？',
      success: function (res) {
        if (res.confirm) {
          // console.log('用户点击确定');
          wx.setStorageSync('disharr', []);
          that.setData({ payDish: wx.getStorageSync('disharr') });
          that.setData({ select2: '1' });
          that.setData({ totalPrice: "0" });
          that.cartImg();
        }
      }
    });
    this.cartImg();
  },
  countPrice: function () {//计算所有金额
    var totalPrice = 0.00;
    for (var i = 0; i < this.data.payDish.length; i++) {
      totalPrice += this.data.payDish[i].num * this.data.payDish[i].price;
    }
    this.setData({ totalPrice: totalPrice.toFixed(2) });
  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  placeOrder: function () {
    var num = this.data.tapNum;
    num++;
    this.setData({ tapNum: num })
    console.log(num);
    if (this.data.tapNum != 1) {
      return;
    }
    if(wx.getStorageSync('sid') == ""){
      wx.navigateTo({
        url: '../bindtel/bindtel?type=1'
      })
      return;
    }
    if (this.data.payDish.length == 0) {
      wx.showToast({
        title: '您尚未选择菜品,马上来一单吧！',
        duration: 2000
      })
      this.setData({ tapNum: 0 })
      return;
    } else {
      wx.navigateTo({
        url: '../confirmOrder/confirmOrder'
      })
    }
  },
  gotel: function (e) {
    var tel = e.currentTarget.dataset.tel;
    wx.makePhoneCall({
      phoneNumber: tel //仅为示例，并非真实的电话号码
    })
  },
  errorImg: function (e) {
    var index1 = e.currentTarget.dataset.indexo;
    var index2 = e.currentTarget.dataset.indext;
    var dishDetails = this.data.dishDetails;
    dishDetails[index1].goods[index2].goodsPic = "../../asset/image/DefaultAvatar.png";
    this.setData({ dishDetails: dishDetails });
  }
})