// pages/search/search.js
Page({
  data: {
    searchContent: "",
    findRecord: []
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
  },
  onReady: function () {
    // 页面渲染完成
    if (wx.getStorageSync('findRecord') == undefined || wx.getStorageSync('findRecord') == "") {
      this.setData({
        findRecord: []
      })
    } else {
      this.setData({
        findRecord: wx.getStorageSync('findRecord')
      })
    }

  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  onPullDownRefresh: function () {//禁止下拉
    wx.stopPullDownRefresh();
  },
  search: function () {//点击搜索按钮
    var _this = this.data.searchContent;
    var _that = this.data.findRecord;
    var recordAll = wx.getStorageSync('findRecord');
    if (_this != "" && recordAll.indexOf(_this, 0) == -1) {
      _that.unshift(_this);
      wx.setStorageSync('findRecord', _that);
      this.setData({
        findRecord: wx.getStorageSync('findRecord')
      })
    }
  },
  inputContent: function (e) {//监听输入事件，获取输入内容
    var search = e.detail.value;
    wx.setStorageSync('searchContent', search);
    this.setData({
      searchContent: search
    })
  },
  clearSearch: function () {//清除全部记录
    wx.setStorageSync('findRecord', []);
    this.setData({
      findRecord: wx.getStorageSync('findRecord')
    })
  },
  clearOne: function (e) {//清除一条记录
    const msg = e.currentTarget.dataset.msg;
    var arr = wx.getStorageSync('findRecord');
    for (var i = 0; i < arr.length; i++) {
      if (msg == arr[i]) {
        arr.splice(i, 1);
        break;
      }
    }
    wx.setStorageSync('findRecord', arr);
    this.onReady();
  }
})