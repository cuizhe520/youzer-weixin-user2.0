//index.js
//获取应用实例
var app = getApp();
Page({
  //页面内数据
  data: {
    // 图片设置和路径
    array: [{
      mode: 'scaleToFill',
      text: 'scaleToFill：不保持纵横比缩放图片，使图片完全适应'
    }],
    src: '../images/cart_hover.png',
    // 轮播图相关设置
    show: 0,
    address: ["南楼", "营口道"],
    address1: ["dddd", "ffff", "dfdf", "asdf"],
    //筛选提交参数
    screendata: {
      add: 1,
      type: 1,
      distance: 1,
      price: 1
    },
    windowHeight: "",
    windowWidth: "",
    indicatorDots: true,
    autoplay: true,
    circular: false,
    interval: 4000,
    duration: 1000,
    //商家列表数据
    merchantList: [],
    page: 1,

    siftingsTypeId: 1,
    countyId: 0,
    typeId: 0,
    bizareaId: 0,

    Bizarea: [],
    i: 0,//控制地区
    j: 0,
    k: 0,
    h: 0,
    screen1: "全部商圈",
    screen2: "全部类别",
    screen3: "距离最近",
    typeList: [],
    distanceList: [
      { name: "距离最近", id: 1 },
      { name: "人均最低", id: 2 },
      { name: "人均最高", id: 3 },
      { name: "评分最高", id: 4 }
    ],
    all: 0
  },
  //加载后执行
  onLoad: function () {
    // console.log('onLoad')
    var that = this
    var lat = "";
    var lng = "";
    var sendValue;
    //调用应用实例的方法获取全局数据
    wx.getSystemInfo({
      success: function (res) {
        // console.log(res.windowWidth)
        // console.log(res.windowHeight)
        that.setData({ windowHeight: res.windowHeight - 42 })
        that.setData({ windowWidth: res.windowWidth })
      }
    })
    app.getUserInfo(function (userInfo) {
      //更新数据
      that.setData({
        userInfo: userInfo
      })
    })
    wx.getLocation({
      type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        // success
        wx.setStorageSync('lat', res.latitude);
        wx.setStorageSync('lng', res.longitude);
        sendValue = {
          command: "GetAllMerchant",
          cityId: "607",
          page: that.data.page,
          perPage: 8,
          lat: res.latitude,
          lng: res.longitude
        };
        var jsonValue = JSON.stringify(sendValue);
        that.requestmerchant(jsonValue)
      }, fail: function () {
        sendValue = {
          command: "GetAllMerchant",
          cityId: "607",
          page: that.data.page,
          perPage: 8
        };
        var jsonValue = JSON.stringify(sendValue);
        that.requestmerchant(jsonValue);
      }
    })
    var sendValue1 = {
      command: "GetMiniWeChatShowBizarea"
    };
    var jsonValue1 = JSON.stringify(sendValue1);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue1
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        // console.log(res);
        if (res.data.code == 1) {
          that.setData({ Bizarea: res.data.data.Bizarea });
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
    //
    var sendValue1 = {
      command: "GetMerchantGoodsType"
    };
    var jsonValue1 = JSON.stringify(sendValue1);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue1
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        // console.log(res);
        if (res.data.code == 1) {
          that.setData({ typeList: res.data.data.goods_type });
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  onShow: function () {
    wx.setStorageSync('disharr', []);
  },
  requestmerchant: function (jsonValue) {
    var that = this;
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res)
        if (res.data.code == 1) {
          if (res.data.data.AllMerchantList.length == 0) {
            wx.showToast({
              title: '没有更多了',
              duration: 2000
            })
            return;
          }
          var list = that.data.merchantList;
          list = list.concat(res.data.data.AllMerchantList);
          that.setData({ merchantList: list })
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  //商家跳转
  tap001: function (e) {
    var that = this;
    const merchantId = e.currentTarget.dataset.merchantid;
    // console.log(wx.getStorageSync('sid'));
    // if (wx.getStorageSync('sid') != "") {
    wx.navigateTo({
      url: '../order/order?merchantId=' + merchantId
    })
    // }else{
    //   wx.navigateTo({
    //     url: '../bindtel/bindtel'
    //   })
    // }
  },
  //搜索跳转
  search: function () {
    wx.navigateTo({
      url: '../template/search/search'
    })
  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  //筛选
  screen: function (e) {
    const index = e.currentTarget.dataset.index;
    if (index == 1 && this.data.show != 1) {
      this.setData({ show: "1" })
    } else if (index == 1 && this.data.show == 1) {
      this.setData({ show: "0" })
    } else if (index == 2 && this.data.show != 2) {
      this.setData({ show: "2" })
    } else if (index == 2 && this.data.show == 2) {
      this.setData({ show: "0" })
    } else if (index == 3 && this.data.show != 3) {
      this.setData({ show: "3" })
    } else if (index == 3 && this.data.show == 3) {
      this.setData({ show: "0" })
    }
  },
  loadMore: function () {
    console.log("到底了");
    var that = this;
    var page = that.data.page;
    page++;
    if (that.data.screen1 == "地区" && that.data.screen2 == "全部类别" && that.data.screen3 == "距离最近") {
      that.setData({ page: page });
      var sendValue = {
        command: "GetAllMerchant",
        cityId: "607",
        page: that.data.page,
        perPage: 8,
        lat: wx.getStorageSync('lat'),
        lng: wx.getStorageSync('lng')
      };
      var jsonValue = JSON.stringify(sendValue);
      console.log(jsonValue);
      that.requestmerchant(jsonValue);
    } else {
      that.setData({ page: page });
      that.requestScreen(page);
    }

  },
  chooseCity: function (e) {
    var index = e.currentTarget.dataset.index;
    var pid = e.currentTarget.dataset.pid;
    this.setData({ i: index })
    this.setData({ j: 0, all: pid })
  },
  chooseBizarea: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var name = e.currentTarget.dataset.name;
    var id = e.currentTarget.dataset.id;
    var pid = e.currentTarget.dataset.pid;
    this.setData({ j: index });
    this.setData({ screen1: name, show: 0, countyId: pid, bizareaId: id, page: 1 });
    this.requestScreen(that.data.page);
  },
  chooseType: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var name = e.currentTarget.dataset.name;
    var id = e.currentTarget.dataset.id;
    this.setData({ k: index, show: 0, screen2: name, page: 1, typeId: id });
    this.requestScreen(that.data.page);
  },
  chooseDistance: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var name = e.currentTarget.dataset.name;
    var id = e.currentTarget.dataset.id;
    this.setData({ h: index, show: 0, screen3: name, page: 1, siftingsTypeId: id });
    this.requestScreen(that.data.page);
  },
  requestScreen: function (pagenum) {
    var that = this;
    if (wx.getStorageSync('lat') == "") {
      var sendValue = {
        command: "GetFiltrateMerchant",
        siftingsTypeId: that.data.siftingsTypeId,//人均
        countyId: that.data.countyId,//区id
        typeId: that.data.typeId,//类别
        bizareaId: that.data.bizareaId,//商圈id
        cityId: "607",//城市id
        AverageSpendId: 0,//人均消费
        page: pagenum,
        perPage: 8
      };
    } else {
      var sendValue = {
        command: "GetFiltrateMerchant",
        siftingsTypeId: that.data.siftingsTypeId,//人均
        countyId: that.data.countyId,//区id
        typeId: that.data.typeId,//类别
        bizareaId: that.data.bizareaId,//商圈id
        cityId: "607",//城市id
        AverageSpendId: 0,//人均消费
        lat: wx.getStorageSync('lat'),
        lng: wx.getStorageSync('lng'),
        page: pagenum,
        perPage: 8
      };
    }
    var sendJson = JSON.stringify(sendValue);
    console.log(sendJson)
    wx.request({
      url: app.requestUrl2,
      data: {
        data: sendJson
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        // console.log(res);
        if (res.data.code == 1) {

          if (pagenum == 1) {
            that.setData({ merchantList: res.data.data.merchantList })
          } else {
            if (res.data.data.merchantList.length == 0) {
              wx.showToast({
                title: '没有更多了',
                duration: 2000
              })
              return;
            }
            var list = that.data.merchantList;
            list = list.concat(res.data.data.merchantList);
            that.setData({ merchantList: list })
          }
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete

      }
    })
  },
  scancode:function(){
    wx.scanCode({
      success: function(res) {
        console.log(res);
        var merchantid = res.result.split(";")[0];
        wx.navigateTo({
          url: '../order/order?merchantId=' + merchantid
        })
      },
      fail: function(res) {
        // console.log(res)
      },
      complete: function(res) {
        // console.log(res)
      },
    })
  },
  errorImg:function(e){
    var index = e.currentTarget.dataset.index;
    var merchantlist = this.data.merchantList;
    merchantlist[index].pic = "../../asset/image/DefaultAvatar.png";
    this.setData({ merchantList:merchantlist});
  }
});
