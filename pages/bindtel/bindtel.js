// pages/bindtel/bindtel.js
var app = getApp();
var md5 = require("../../utils/md5.js");
Page({
  data: {
    tel: "",
    code: "",
    time: -1,
    placeholder: "请输入手机验证码",
    title: "验证码",
    panduan: 0,
    regCode: "",
    code1: "",
    eventName: "getcode",
    type: "",
    tapNum: 0
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({ type: options.type });
  },
  onReady: function () {
    // 页面渲染完成
    var that = this;
    wx.login({
      success: function (res) {
        // success
        that.setData({ code1: res.code })
        console.log(res);
      },
      fail: function (res) {
        // fail
        console.log(res)
      },
      complete: function (res) {
        // complete
      }
    })
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
    if (this.data.type == 0) {
      wx.switchTab({
        url: '../index/index',
        success: function (res) {
          // success
        },
        fail: function (res) {
          // fail
        },
        complete: function (res) {
          // complete
        }
      })
    }

  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  inputtel: function (e) {
    console.log(e.detail.value)
    var tel = e.detail.value;
    this.setData({ tel: tel });
  },
  inputcode: function (e) {
    console.log(e.detail.value)
    var code = e.detail.value;
    this.setData({ code: code });
  },
  bindtel: function (e) {
    var that = this;
    var tel = that.data.tel;
    var code = that.data.code;
    var msg = /^(0|86|17951)?1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)[0-9]{8}$/;
    var code1 = this.data.code1;
    if (!msg.test(tel)) {
      wx.showToast({
        title: '手机号码格式错误',
        duration: 2000
      })
      return;
    }
    console.log(e.detail.value);
    // wx.checkSession({
    //   success: function () {
    //     //session 未过期，并且在本生命周期一直有效
    //   },
    //   fail: function () {
    //     //登录态过期
    //     //重新登录

    // }
    // })
    var sendValue = {
      command: "MobileNoLogin",
      mobileNo: tel,
      verifyCode: md5.hexMD5(code),
      code: code1
    }
    var sendJson = JSON.stringify(sendValue);
    console.log(sendJson)
    wx.request({
      url: app.requestUrl2,
      data: {
        data: sendJson
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          wx.showToast({
            title: '登录成功',
            duration: 2000
          })
          wx.setStorageSync('sid', res.data.data.sid);
          wx.setStorageSync('userInfo', res.data.data.userInfo);
          if (that.data.type == 0) {
            wx.switchTab({
              url: '../index/index'
            })
          }else if (that.data.type == 1) {
            wx.navigateBack({
              delta: 1, // 回退前 delta(默认为1) 页面
              success: function (res) {
                // success
              },
              fail: function (res) {
                // fail
              },
              complete: function (res) {
                // complete
              }
            })
          }
        } else if (res.data.code == -9) {
          wx.showToast({
            title: '验证码错误或已过期',
            duration: 2000
          })
          return;
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  getcode: function () {
    var that = this;
    var tel = that.data.tel;
    var msg = /^(0|86|17951)?1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)[0-9]{8}$/;
    var j = 60;
    var num = this.data.tapNum;

    if (!msg.test(tel)) {
      wx.showToast({
        title: '手机号码格式错误',
        duration: 2000
      })
      return;
    }
    num++;
    this.setData({ tapNum: num })
    if (this.data.tapNum != 1) {
      return;
    }
    var sendValue = {
      command: "GetNewRegCode",
      mobileNo: tel
    }
    var sendJson = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: sendJson
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        
        if (res.data.code == 1) {

          that.setData({ eventName: "dddd" })
          var timer = setInterval(function () {
            j--;
            that.setData({ time: j });
            if (j == -1) {
              j = 60;
              that.setData({ eventName: "getcode" });
              that.setData({ time: -1 });
              that.setData({ tapNum: 0 })
              clearInterval(timer);
            }
          }, 1000)
        } else if (res.data.code == -15) {
          that.setData({ placeholder: "请输入图形验证码" });
          that.setData({ panduan: 1 });
          that.random();
        } else if (res.data.code == -14) {
          wx.showToast({
            title: '验证码错误',
            duration: 2000
          })
          return;
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  inputImgcode: function (e) {
    var that = this;
    var tel = that.data.tel;
    var j = 60;
    if (that.data.panduan != 0 && e.detail.value.length == 4) {
      var sendValue = {
        command: "GetNewRegCode",
        mobileNo: tel,
        captchaNo: md5.hexMD5(e.detail.value.toLowerCase())
      }
      var sendJson = JSON.stringify(sendValue);
      console.log(sendJson)
      wx.request({
        url: app.requestUrl2,
        data: {
          data: sendJson
        },
        method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
        success: function (res) {
          // success
          console.log(res);
          if (res.data.code == 1) {
            that.setData({ panduan: 0 });
            that.setData({ placeholder: "请输入验证码" });
            var timer = setInterval(function () {
              j--;
              that.setData({ time: j });
              if (j == -1) {
                j = 60;
                that.setData({ time: -1 })
                clearInterval(timer);
              }
            }, 1000)
          } else if (res.data.code == -15) {
            that.setData({ placeholder: "请输入图文验证码" });
            that.setData({ panduan: 1 })
          } else if (res.data.code == -14) {
            wx.showToast({
              title: '验证码错误',
              duration: 2000
            })
            return;
          }
        },
        fail: function (res) {
          // fail
        },
        complete: function (res) {
          // complete
        }
      })
    }
  },
  random: function () {
    var num = Math.floor(Math.random() * 1000) + 1;
    var tel = this.data.tel;
    this.setData({ regCode: "https://wxuser.youzer.cn/api.php/GetCaptcha?mobileNo=" + tel + "&" + num });
  }
})