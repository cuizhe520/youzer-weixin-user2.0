// pages/details/details.js
var QR = require("../../utils/qrcode.js");
var app = getApp();
Page({
  data: {
    placeholder: "",
    stateImgurl: "../../asset/image/pic_gr_df.png",
    listdata: [],
    orderInfo: [],
    select: ""
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    console.log(options);
    var that = this;
    var sid = wx.getStorageSync('sid');
    that.setData({select:options.ordertype});
    if (options.ordertype == 0) {
      that.setData({ stateImgurl: "../../asset/image/pic_gr_df.png" });
    } else if (options.ordertype == 1) {
      that.setData({ stateImgurl: "../../asset/image/pic_gr_sy.png" });
    } else if (options.ordertype == 2) {
      that.setData({ stateImgurl: "../../asset/image/pic_gr_wc.png" });
    }
    var sendValue = {
      command: "GetOrderFoods",
      sid: sid,
      orderId: options.orderid
    };
    var jsonValue = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        that.setData({ placeholder: res.data.data.order_info.exchangeCode });
        that.setData({ listdata: res.data.data.order_detail });
        that.setData({ orderInfo: res.data.data.order_info });
        that.createQrCode(that.data.placeholder, "mycanvas", 180, 180);
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  onReady: function () {
    // 页面渲染完成
    // var size = this.setCanvasSize();//动态设置画布大小
    // var initUrl = this.data.placeholder;
    // this.createQrCode(initUrl, "mycanvas", 180, 180);
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  // setCanvasSize:function(){
  //   var size={};
  //   try {
  //       var res = wx.getSystemInfoSync();
  //       var scale = 750/686;//不同屏幕下canvas的适配比例；设计稿是750宽
  //       var width = res.windowWidth/scale;
  //       var height = width;//canvas画布为正方形
  //       size.w = width;
  //       size.h = height;
  //     } catch (e) {
  //       // Do something when catch error
  //       console.log("获取设备信息失败"+e);
  //     } 
  //   return size;
  // } ,
  createQrCode: function (url, canvasId, cavW, cavH) {
    //调用插件中的draw方法，绘制二维码图片
    QR.qrApi.draw(url, canvasId, cavW, cavH);
  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  phone: function () {
    wx.makePhoneCall({
      phoneNumber: '02283658377' 
    })
  },
  gopay:function(){
    var paylist = this.data.orderInfo;
    var orderdetails = {
      orderNumber: paylist.orderNumber,
      userName: paylist.realName,
      mobile: paylist.mobileNo,
      total: paylist.total_fee,
      point: paylist.sumPoint,
      orderId: paylist.id
    }
    console.log(orderdetails);
    wx.setStorageSync('paydish', orderdetails);
    wx.navigateTo({
      url: '../pay/pay'
    })
  }
})