// pages/confirmOrder/confirmOrder.js
var app = getApp();
// app.tofixed();
Page({
  data: {
    placeholder: "1212121312",
    stateImgurl: "../../asset/image/pic_gr_df.png",
    listdata: [
      { dishname: "asdkjfk", num: "12", price: "12", score: "1" },
      { dishname: "asdkjfk", num: "12", price: "12", score: "1" },
      { dishname: "asdkjfk", num: "12", price: "12", score: "1" }
    ],
    totalPrice: "",
    userInfo: [],
    ordersList: [],
    merchantName:"",
    tapNum:0
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.setData({ userInfo: wx.getStorageSync('userInfo') });
    this.setData({ listdata: wx.getStorageSync('disharr') });
    this.setData({ merchantName: wx.getStorageSync('merchantName') });
    this.priceSum();
    var ordersList = [];
    var list = wx.getStorageSync('disharr');
    for (var i = 0; i < list.length; i++) {
      ordersList[i] = {
        goodsId: list[i].goodsid,
        num: list[i].num,
        activityTypeId: "0",
        activityNo: "0"
      }
    }
    this.setData({ ordersList: ordersList });
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
    
  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  //支付
  goPay: function () {
    var sid = wx.getStorageSync('sid');
    var ordersList = this.data.ordersList;
    var merchantid = wx.getStorageSync('merchantid');
    var num = this.data.tapNum;
    num++;
    this.setData({ tapNum: num })
    if (this.data.tapNum != 1) {
      return;
    }
    var sendValue = {
      command: "DealOrder",
      activityNo: "0",
      sid: sid,
      ordersList: ordersList,
      merchantId: merchantid
    };
    var jsonValue = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          wx.setStorageSync('paydish', res.data.data);
          wx.navigateTo({
            url: '../pay/pay'
          })
        }else if(res.data.code == -10){
          wx.setStorageSync('sid', "");
          wx.showToast({
            title: '登录已过期，请重新登录',
            duration: 2000
          });
          wx.clearStorage();
        }
      },
      fail: function (res) {
        // fail
        this.setData({ tapNum: 0 })
      },
      complete: function (res) {
        // complete
      }
    })
  },
  priceSum: function () {
    var that = this;
    var totalPrice = 0;
    for (var i = 0; i < that.data.listdata.length; i++) {
      totalPrice += that.data.listdata[i].num * that.data.listdata[i].price;
    }
    this.setData({ totalPrice: totalPrice.toFixed(2) });
  }
})