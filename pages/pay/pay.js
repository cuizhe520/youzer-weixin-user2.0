// pages/pay/pay.js
var app = getApp();
var md5 = require("../../utils/md5.js");
Page({
  data: {
    confirmImgUrl: "../../asset/image/content_icon_circle-.png",
    list: [],
    left: "",
    show: 0,
    tapNum:0
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    var list = wx.getStorageSync('paydish');
    var that = this;
    this.setData({ list: list });
    wx.getSystemInfo({
      success: function (res) {
        console.log(res.windowWidth)
        console.log(res.windowHeight)
        that.setData({ left: res.windowWidth / 2 - 80 });
      }
    })
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
    console.log("页面已经关闭了")
    wx.switchTab({
      url: '../orderDetail/detail'
    })
  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  confirmImg: function () {//点击同意免责声明
    if (this.data.confirmImgUrl == "../../asset/image/content_icon_circle-.png") {
      this.setData({ confirmImgUrl: "../../asset/image/content_icon_check.png" })
    } else {
      this.setData({ confirmImgUrl: "../../asset/image/content_icon_circle-.png" })
    }
  },
  pay: function () {
    var sid = wx.getStorageSync('sid');
    var num = this.data.tapNum;
    var that =this;
    if (this.data.confirmImgUrl != "../../asset/image/content_icon_check.png") {
      wx.showToast({
        title: '请您同意免责声明',
        duration: 2000
      })
      return;
    }
    num++;
    this.setData({ tapNum: num })
    if (this.data.tapNum != 1) {
      return;
    }
    var sendValue = {
      orderId: this.data.list.orderId,//"494910",
      command: "WxMiniPrepay",
      sid: sid,//"npmuvj5flfd1ph46n9g89jf6h1",
      userCouponNo: "0"
    }
    var sendJson = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: sendJson
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          var paysign = "appId=" + res.data.data.appid + "&nonceStr=" + res.data.data.nonce_str + "&package=prepay_id=" + res.data.data.prepay_id + "&signType=MD5&timeStamp=" + res.data.data.timenum + "&key=fhehshektnzutmfhakrnvhabfrxz232a";
          paysign = md5.hexMD5(paysign);
          paysign = paysign.toUpperCase();
          console.log(paysign);
          wx.requestPayment({
            'timeStamp': String(res.data.data.timenum),
            'nonceStr': res.data.data.nonce_str,
            'package': "prepay_id=" + res.data.data.prepay_id,
            'signType': 'MD5',
            'paySign': paysign,
            'success': function (res) {
              console.log(res)
              wx.switchTab({
                url: '../orderDetail/detail'
              })
            },
            'fail': function (res) {
              console.log(res);
              that.setData({ tapNum: 0 })
            }
          })
        }
      },
      fail: function (res) {
        // fail
        this.setData({ tapNum: 0 })
      },
      complete: function (res) {
        // complete
      }
    })

  },
  close:function(){
    this.setData({show:0})
  },
  confirm:function(){
    this.setData({ confirmImgUrl: "../../asset/image/content_icon_check.png" });
    this.close();
  },
  alert:function(){
    this.setData({show:1})
  }
})