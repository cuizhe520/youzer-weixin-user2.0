// pages/orderDetail/detail.js
var app = getApp();
Page({
  data: {
    stateList: [
      "待付款", "待使用", "已完成"
    ],
    select: "0",
    orderList: [],
    paidList: [],
    completeList: [],
    state: "",//已完成订单状态
    windowHeight: "",
    pagenum: 1,
    left: ""
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        console.log(res.windowWidth)
        console.log(res.windowHeight)
        that.setData({ windowHeight: res.windowHeight + 5});
        that.setData({ left: res.windowWidth / 2 - 40 })
      }
    })
    console.log(wx.getStorageSync('sid'))
  },
  onReady: function () {
    // 页面渲染完成
    console.log("1");

  },
  onShow: function () {
    // 页面显示
    console.log("2");
    var that = this;
    if (wx.getStorageSync('sid') == "") {
      wx.showModal({
        title: '提示',
        content: '您尚未登录，是否登录？',
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '../bindtel/bindtel?type=0'
            })
          } else if (res.cancel) {

          }
        }
      })
      return;
    } else {
      wx.setStorageSync('paydish', []);
      if (that.data.select == 0) {
        that.requestNot(1);
      } else if (that.data.select == 1) {
        that.requestpaid(1);
      } else if (that.data.select == 2) {
        that.requestcomplete(1);
      }
    }

  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  requestNot: function (pagenum) {
    var that = this;
    var sid = wx.getStorageSync('sid');
    var sendValue = {
      command: "GetUserFoodsOrderNotPaid",
      sid: sid,
      page: pagenum,
      perPage: "10"
    }
    var jsonValue = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          if (pagenum == 1) {
            that.setData({ orderList: res.data.data.orderList })
            that.setData({ pagenum: 1 })
          } else {
            var list = that.data.orderList;
            list = list.concat(res.data.data.orderList);
            that.setData({ orderList: list })
          }
        } else if (res.data.code == -10) {
          wx.setStorageSync('sid', "");
          wx.showToast({
            title: '登录已过期，请重新登录',
            duration: 2000
          });
          wx.clearStorage();
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  requestpaid: function (pagenum) {
    var that = this;
    var sid = wx.getStorageSync('sid');
    var sendValue = {
      command: "GetUserFoodsOrderAlreadyPaid",
      sid: sid,
      page: pagenum,
      perPage: "10"
    }
    var jsonValue = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          if (pagenum == 1) {
            that.setData({ paidList: res.data.data.orderList })
            that.setData({ pagenum: 1 })
          } else {
            var list = that.data.paidList;
            list = list.concat(res.data.data.orderList);
            that.setData({ paidList: list })
          }
        } else if (res.data.code == -10) {
          wx.setStorageSync('sid', "");
          wx.showToast({
            title: '登录已过期，请重新登录',
            duration: 2000
          });
          wx.clearStorage();
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  requestcomplete: function (pagenum) {
    var that = this;
    var sid = wx.getStorageSync('sid');
    var sendValue = {
      command: "GetUserFoodsOrdeFinish",
      sid: sid,
      page: pagenum,
      perPage: "10"
    }
    var jsonValue = JSON.stringify(sendValue);
    wx.request({
      url: app.requestUrl2,
      data: {
        data: jsonValue
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res);
        if (res.data.code == 1) {
          if (pagenum == 1) {
            that.setData({ completeList: res.data.data.orderList })
            that.setData({ pagenum: 1 })
          } else {
            var list = that.data.completeList;
            list = list.concat(res.data.data.orderList);
            that.setData({ completeList: list })
          }
        } else if (res.data.code == -10) {
          wx.setStorageSync('sid', "");
          wx.showToast({
            title: '登录已过期，请重新登录',
            duration: 2000
          });
          wx.clearStorage();
        }
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  tap001: function (e) {
    const index = e.currentTarget.dataset.index;
    this.setData({
      select: index
    })
    if (index == 0) {
      this.requestNot(1);
    } else if (index == 1) {
      this.requestpaid(1);
    } else if (index == 2) {
      this.requestcomplete(1);
    }
  },
  //是否开启下拉刷新
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  jump: function (e) {
    var that = this;
    var orderid = e.currentTarget.dataset.orderid;
    wx.navigateTo({
      url: '../details/details?orderid=' + orderid + "&ordertype=" + that.data.select
    })
  },//删除订单
  delOrder: function (e) {
    var sid = wx.getStorageSync('sid');
    var orderid = e.currentTarget.dataset.orderid;
    var index = e.currentTarget.dataset.index;
    var that = this;
    var sendValue = {
      command: "DelOrder",
      orderId: orderid,
      sid: sid
    };
    var jsonValue = JSON.stringify(sendValue);
    wx.showModal({
      title: '提示',
      content: '是否删除订单？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.requestUrl2,
            data: {
              data: jsonValue
            },
            method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
            success: function (res) {
              // success
              console.log(res);
              if (res.data.code == 1) {
                //删除data里面的数据
                var list = that.data.completeList;
                list.splice(index, 1);
                that.setData({ completeList: list });
              } else if (res.data.code == -10) {
                wx.setStorageSync('sid', "");
                wx.showToast({
                  title: '登录已过期，请重新登录',
                  duration: 2000
                });
                wx.clearStorage();
              }
            },
            fail: function (res) {
              // fail
            },
            complete: function (res) {
              // complete
            }
          })
        } else if (res.cancel) {

        }
      }
    })


  },//取消订单
  cancelOrder: function (e) {
    var sid = wx.getStorageSync('sid');
    var orderid = e.currentTarget.dataset.orderid;
    var index = e.currentTarget.dataset.index;
    var that = this;
    var sendValue = {
      command: "CancelOrder",
      orderId: orderid,
      sid: sid
    };
    var jsonValue = JSON.stringify(sendValue);
    wx.showModal({
      title: '提示',
      content: '是否取消订单？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: app.requestUrl2,
            data: {
              data: jsonValue
            },
            method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: { 'content-Type': 'application/x-www-form-urlencoded' }, // 设置请求的 header
            success: function (res) {
              // success
              if (res.data.code == 1) {
                var list = that.data.orderList;
                list.splice(index, 1);
                that.setData({ orderList: list });
              } else if (res.data.code == -10) {
                wx.setStorageSync('sid', "");
                wx.showToast({
                  title: '登录已过期，请重新登录',
                  duration: 2000
                });
                wx.clearStorage();
              }
            },
            fail: function (res) {
              // fail
            },
            complete: function (res) {
              // complete
            }
          })
        } else if (res.cancel) {

        }
      }
    })
    
  },//去支付
  goPay: function (e) {
    var index = e.currentTarget.dataset.index;
    var paylist = this.data.orderList;
    var orderdetails = {
      orderNumber: paylist[index].orderNumber,
      userName: paylist[index].realName,
      mobile: paylist[index].mobileNo,
      total: paylist[index].total_fee,
      point: paylist[index].sumPoint,
      orderId: paylist[index].id
    }
    console.log(orderdetails)
    wx.setStorageSync('paydish', orderdetails);
    wx.navigateTo({
      url: '../pay/pay'
    })
  },
  loadMore: function () {
    console.log("到底了吗");
    var pagenum = this.data.pagenum;
    var index = this.data.select;
    pagenum++;
    this.setData({ pagenum: pagenum });
    if (index == 0) {
      this.requestNot(this.data.pagenum);
    } else if (index == 1) {
      this.requestpaid(this.data.pagenum);
    } else if (index == 2) {
      this.requestcomplete(this.data.pagenum);
    }
  },
  errorImg: function (e) {
    var index = e.currentTarget.dataset.index;
    var orderList = this.data.orderList;
    var paidList = this.data.paidList;
    var completeList = this.data.completeList;
    if (this.data.select == 0) {
      orderList[index].pic = "../../asset/image/DefaultAvatar.png";
      this.setData({ orderList: orderList });
    } else if (this.data.select == 1) {
      paidList[index].pic = "../../asset/image/DefaultAvatar.png";
      this.setData({ paidList: paidList });
    } else if (this.data.select == 2) {
      completeList[index].pic = "../../asset/image/DefaultAvatar.png";
      this.setData({ completeList: completeList });
    }
  }
})