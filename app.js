//app.js
App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
  },
  getUserInfo: function (cb) {
    var that = this
    if (this.globalData.userInfo) {
      typeof cb == "function" && cb(this.globalData.userInfo)
      console.log("1");
    } else {
      //调用登录接口
      // wx.login({
      //   success: function (res) {
      //     console.log(res)
      //     var sendValue = {
      //       command: "WxLogin",
      //       code: res.code
      //     };
      //     var jsonValue = JSON.stringify(sendValue);
      //     console.log(jsonValue)
      //     wx.request({
      //       url: that.requestUrl2,
      //       data: {
      //         data:jsonValue
      //       },
      //       method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      //       header: {'content-Type':'application/x-www-form-urlencoded'}, // 设置请求的 header
      //       success: function(res){
      //         // success
      //         console.log(res);
      //       },
      //       fail: function(res) {
      //         // fail
      //       },
      //       complete: function(res) {
      //         // complete
      //       }
      //     })

      //     // wx.getUserInfo({
      //     //   success: function (res) {
      //     //     console.log(res);
      //     //     that.globalData.userInfo = res.userInfo
      //     //     typeof cb == "function" && cb(that.globalData.userInfo)
      //     //   }
      //     // })
      //   }
      // })
    }

  },
  // requestUrl1: "https://app.youzer.cn/youze_merchant/api.php",//老https://e.youzer.cn/youze_merchant/api.php
  requestUrl2: "https://wxuser.youzer.cn/api.php",//新
  globalData: {
    userInfo: null
  },
  tofixed: function () {
    Number.prototype.round = function (len) {
      var old = this;
      var a1 = Math.pow(10, len) * old;
      a1 = Math.round(a1);
      var oldstr = old.toString()
      var start = oldstr.indexOf(".");
      if (start > 0 && oldstr.split(".")[1].length == len + 1) {
        if (oldstr.substr(start + len + 1, 1) == 5) {
          var flagval = oldstr.substr(start + len, 1) - 0;
          if (flagval % 2 == 0) {
            a1 = a1 - 1;
          }
        }
      }
      var b1 = a1 / Math.pow(10, len);
      return b1;
    }
    Number.prototype.oldtoFixed = Number.prototype.toFixed;
    Number.prototype.toFixed = function (len) {
      var old = this;
      var oldstr = old.toString()
      var start = oldstr.indexOf(".");
      if (len == 2 && start > 0 && oldstr.split(".")[1].length == 3) {
        return this.round(len);
      }
      else {
        return this.oldtoFixed(len);
      }
    }
  }
})